﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Pawn : MonoBehaviour 
{
	// Pawn tag (enemy or friendly)
	public GameManager.Tag pawnTag;
	// The type of pawn it is
	public enum PawnType { Rock, Paper, Scissors }
	public PawnType pawnType;

	// The lane the pawn is currently in along with some components
	public SpawnPoint.SpawnLane pawnLane;
	public Collider pawnCollider;
	public NavMeshAgent agent;

	// The enemy base to check distance for attack
	private Base friendlyBase;
	private Base enemyBase;

	/// <summary>
	/// This is an initialization function for the pawn which sets it's values
	/// </summary>
	/// <param name="pawnTag"></param>
	/// <param name="pawnLane"></param>
	public void SetupPawn(GameManager.Tag pawnTag, SpawnPoint.SpawnLane pawnLane) 
	{
		this.pawnTag = pawnTag;
		this.pawnLane = pawnLane;

		// If pawn belongs to player
		if(pawnTag == GameManager.Tag.player) 
		{
			friendlyBase = GameManager.instance.PlayerBase;
			enemyBase = GameManager.instance.EnemyBase;
		}
		// If pawn belongs to AI
		else 
		{
			friendlyBase = GameManager.instance.EnemyBase;
			enemyBase = GameManager.instance.PlayerBase;
		}
	}

	/// <summary>
	/// This enables the pawn
	/// </summary>
	public void EnablePawn() 
	{
		pawnCollider.enabled = true;
		agent.enabled = true;
	}

	/// <summary>
	/// This disables the pawn so that it doesn't walk or trigger anything
	/// </summary>
	public void DisablePawn()
	{
		pawnCollider.enabled = false;
		agent.enabled = false;
	}

	/// <summary>
	/// Set the target for the nav agent
	/// </summary>
	/// <param name="pos"></param>
	public void SetTarget(Vector3 pos) 
	{
		agent.destination = pos;
	}

	void Update()
	{
		// Goes through all the spawned pawns
		foreach(var otherPawn in GameManager.instance.spawnedPawns.ToArray()) 
		{
			// Checks if the pawn is an enemy and the distance between them
			if(otherPawn.pawnTag != pawnTag && otherPawn.pawnLane == pawnLane && Vector3.Distance(transform.position, otherPawn.transform.position) < 2f) 
			{
				if(pawnType == otherPawn.pawnType)
				{
					otherPawn.Defeat();
					Defeat();
				}

				if((pawnType == PawnType.Rock && otherPawn.pawnType == PawnType.Paper) ||
					(pawnType == PawnType.Paper && otherPawn.pawnType == PawnType.Scissors) ||
					(pawnType == PawnType.Scissors && otherPawn.pawnType == PawnType.Rock))
					{
						otherPawn.Defeat();
						Defeat();
						friendlyBase.DoDamage();
					}
			}
		}

		// Check distance from enemy base to see if you can attack it
		if(enemyBase != null && Vector3.Distance(transform.position, enemyBase.transform.position) < 5.0f) {
			// Do damage then remove the player
			enemyBase.DoDamage();
			Defeat();
		}
	}

	/// <summary>
	/// This removes the pawn from the game
	/// </summary>
	public void Defeat()
	{
		GameManager.instance.spawnedPawns.Remove(this);
		Destroy(gameObject);
	}

	/// <summary>
	/// Draw a gizmo to display the attack distance for the pawn
	/// </summary>
	void OnDrawGizmos()
	{
		Color gizmoColor = Color.red;
		gizmoColor.a = 0.2f;
		Gizmos.color = gizmoColor;
		Gizmos.DrawSphere(transform.position, 2f);	
	}
}
