﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;
using DG.Tweening;

public class ChoiceManager : MonoBehaviour 
{
	public static ChoiceManager instance;

	public List<SpawnOption> spawnOptions = new List<SpawnOption>();

	private float startYPos = 0.0f;
	private float endYPos = 60.0f;
	private bool bDragging = false;
	private GameObject spawnedObject;

	void Awake()
	{
		if(instance != null && instance != this)
			Destroy(gameObject);
		
		instance = this;
	}

	public void PointerEnter(GameObject go) 
	{
		go.transform.DOMoveY(endYPos, 1.0f);
	}

	public void PointerExit(GameObject go)
	{
		go.transform.DOMoveY(startYPos, 1.0f);
	}

	public void PointerDown(string key)
	{
		bDragging = true;
		spawnedObject = GameObject.Instantiate(spawnOptions.FirstOrDefault(x=>x.key==key).prefab, Vector3.zero, Quaternion.identity);
		spawnedObject.GetComponent<Pawn>().DisablePawn();
	}

	public void PointerUp(string key) 
	{
		if(bDragging)
		{
			bDragging = false;
			Destroy(spawnedObject);
			GameManager.instance.PlayerBase.SpawnPawn(spawnOptions.FirstOrDefault(x=>x.key==key).prefab, Input.mousePosition);
		}
	}

	private Ray ray = new Ray();
	private RaycastHit hit = new RaycastHit();
	private Vector3 hitPos;
	void FixedUpdate()
	{
		if(bDragging && spawnedObject != null) 
		{
			ray = Camera.main.ScreenPointToRay(Input.mousePosition);
			if(Physics.Raycast(ray, out hit)) {
				hitPos = hit.point;
				hitPos.y += 1f;
				spawnedObject.transform.position = hitPos; 
			}
		}
	}
}

[System.Serializable]
public class SpawnOption 
{
	public string key;
	public GameObject prefab;
}