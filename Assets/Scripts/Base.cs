﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public abstract class Base : MonoBehaviour 
{
	// Health bar
	public Slider healthBar;

	// Health variables
	private float _health = 100.0f;
	public float health {
		get { return _health; } 
		set { 
			_health = value; 
			healthBar.value = _health / maxHealth; 

			if(_health <= 0) Dead();
		}
	}
	public float maxHealth = 100.0f;
	private float damageAmount = 10.0f;

	[Space]

	// The tags for whether it's an enemy pawn or not
	public GameManager.Tag friendlyTag;
	public GameManager.Tag enemyTag;

	[Space]

	// List of spawn points for the pawns (lanes)
	public List<SpawnPoint> spawnPoints = new List<SpawnPoint>();

	/// <summary>
	/// This will attack the base
	/// </summary>
	public void DoDamage() 
	{
		health -= damageAmount;
		GameManager.instance.IncrementScore(enemyTag);
	}

	/// <summary>
	/// This is called when the base is dead
	/// </summary>
	public void Dead()
	{
		GameManager.instance.EndGame(enemyTag);
	}
}


/// <summary>
/// Spawn Point class used to keep track of the spawn points and their lane position
/// </summary>
[System.Serializable]
public class SpawnPoint 
{
	public enum SpawnLane { left, middle, right }
	public SpawnLane spawnLane;
	public GameObject spawnPoint; 
}
