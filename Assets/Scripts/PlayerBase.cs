﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBase : Base 
{
	private Ray ray = new Ray();
	private RaycastHit hit = new RaycastHit();

	/// <summary>
	/// This is the player spawning pawns. It spawns a pawn at the closest spawn point upon stop drag
	/// </summary>
	/// <param name="pawn"></param>
	/// <param name="mousePos"></param>
	public void SpawnPawn(GameObject pawn, Vector3 mousePos) 
	{
		SpawnPoint closestSpawnPoint = spawnPoints[0];
		float lowestDistance = 500000f;

		ray = Camera.main.ScreenPointToRay(Input.mousePosition);
		if(Physics.Raycast(ray, out hit)) {
			foreach(SpawnPoint sPoint in spawnPoints) {
				float dist = Vector3.Distance(hit.point, sPoint.spawnPoint.transform.position);
				if(dist < lowestDistance) {
					closestSpawnPoint = sPoint;
					lowestDistance = dist;
				}
			}
		}

		var go = GameObject.Instantiate(pawn, closestSpawnPoint.spawnPoint.transform.position, Quaternion.identity);
		var pawnComp = go.GetComponent<Pawn>();

		pawnComp.SetupPawn(friendlyTag, closestSpawnPoint.spawnLane);
		pawnComp.SetTarget(GameManager.instance.EnemyBase.transform.position);
		GameManager.instance.EnemyBase.SpawnRandomPawn(closestSpawnPoint.spawnLane);

		GameManager.instance.spawnedPawns.Add(pawnComp);
	}
}
