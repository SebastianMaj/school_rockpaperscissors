﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour 
{
    public static GameManager instance;

    // Tag enum to mark whether something belongs to player or ai
    public enum Tag { player, ai }
    // Enemy and Player base
    public PlayerBase PlayerBase;
    public EnemyBase EnemyBase;

    // List of spawned pawns / rock paper scissors
    public List<Pawn> spawnedPawns = new List<Pawn>();

    // Scoreboard ui variables
    [Header("Scoreboard UI")]
    public Text playerScoreText;
    public Text enemyScoreText;

    // End game UI options
    [Header("Win/Lose UI")]
    public Text winLoseText;
    public Animator winLoseAnimator;
    public Color winColor, loseColor;

    /// <summary>
    /// Awake is called when the script instance is being loaded.
    /// </summary>
    void Awake()
    {
        if(instance != null && instance != this)
            Destroy(gameObject);

        instance = this;
        DontDestroyOnLoad(gameObject);    
    }

    public void IncrementScore(Tag scoreTag) 
    {
        if(scoreTag == Tag.player) playerScoreText.text = (int.Parse(playerScoreText.text) + 1).ToString(); 
        else enemyScoreText.text = (int.Parse(enemyScoreText.text) + 1).ToString(); 
    }

    /// <summary>
    /// This is called when the game is lost
    /// </summary>
    /// <param name="losingTag"></param>
    public void EndGame(Tag winningTag) 
    {
        // If the winning tag is the player
        if(winningTag == Tag.player) 
        {
            winLoseText.text = "WON";
            winLoseText.color = winColor;
        }
        // If the winning tag is the enemy
        else 
        {
            winLoseText.text = "LOST";
            winLoseText.color = loseColor;
        }

        // Open the panel
        winLoseAnimator.Play("Open");
    }

    /// <summary>
    /// Start over
    /// </summary>
    public void PlayAgain()
    {
        SceneManager.LoadScene(0);
    }

    /// <summary>
    /// Closes the game
    /// </summary>
    public void ExitGame() 
    {
        Debug.Log("<color=red>Quitting game</color>");
        Application.Quit();
    }
}
