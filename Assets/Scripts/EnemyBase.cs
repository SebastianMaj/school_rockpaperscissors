﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class EnemyBase : Base 
{
	/// <summary>
	/// The enemy spawns a random pawn (rock | paper | scissors) in the lane chosen
	/// </summary>
	/// <param name="spawnLane"></param>
	public void SpawnRandomPawn(SpawnPoint spawnPoint) 
	{
		GameObject prefab = ChoiceManager.instance.spawnOptions[Random.Range(0, ChoiceManager.instance.spawnOptions.Count)].prefab;

		var go = GameObject.Instantiate(prefab, spawnPoint.spawnPoint.transform.position, Quaternion.identity);
		var pawn = go.GetComponent<Pawn>();

		pawn.SetupPawn(friendlyTag, spawnPoint.spawnLane);
		pawn.SetTarget(GameManager.instance.PlayerBase.transform.position);

		GameManager.instance.spawnedPawns.Add(pawn);
	}
	public void SpawnRandomPawn(SpawnPoint.SpawnLane spawnLane) 
	{
		SpawnPoint spawner = spawnPoints.FirstOrDefault(x => x.spawnLane == spawnLane);
		SpawnRandomPawn(spawner);
	}
}
